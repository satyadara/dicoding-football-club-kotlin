package com.satyadara.footballclubkade

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import com.satyadara.fcapp.FootballClub
import com.satyadara.footballclubkade.Main.FootballClubAdapter
import com.satyadara.footballclubkade.Main.MainActivityUI
import org.jetbrains.anko.setContentView

class MainActivity : AppCompatActivity() {
    private lateinit var rvFootballClub: RecyclerView
    var adapter: FootballClubAdapter? = null
    private var items: MutableList<FootballClub> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainActivityUI().setContentView(this)
        rvFootballClub = findViewById(MainActivityUI.rvFootballClub)
        initData()

        rvFootballClub.adapter = adapter
    }

    private fun initData(){
        val name = resources.getStringArray(R.array.club_name)
        val image = resources.obtainTypedArray(R.array.club_image)
        items.clear()
        for (i in name.indices) {
            items.add(FootballClub(name[i],
                image.getResourceId(i, 0)))
        }

        //Recycle the typed array
        image.recycle()

        adapter = FootballClubAdapter(this, items)
    }
}
