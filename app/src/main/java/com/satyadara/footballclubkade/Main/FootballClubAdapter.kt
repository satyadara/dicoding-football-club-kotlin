package com.satyadara.footballclubkade.Main

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.satyadara.fcapp.FootballClub
import com.satyadara.footballclubkade.Detail.DetailActivity
import com.satyadara.footballclubkade.MainActivity
import com.squareup.picasso.Picasso
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.startActivity

class FootballClubAdapter(private val context: Context, var list: List<FootballClub> = arrayListOf()) :
    RecyclerView.Adapter<FootballClubAdapter.FootballClubVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FootballClubVH {
        return FootballClubVH(FootballClubItemUI().createView(AnkoContext.create(context, MainActivity(), false)))
    }

    override fun onBindViewHolder(holder: FootballClubVH, position: Int) {
        val footballClub = list[position]
        holder.tvNameFC.text = footballClub.name
        Picasso.get()
            .load(footballClub.logo)
            .resize(FootballClubItemUI.logoDipSize, FootballClubItemUI.logoDipSize)
            .into(holder.ivLogoFC)

        holder.itemView.setOnClickListener {
            context.startActivity<DetailActivity>("fc" to footballClub)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class FootballClubVH(itemView: View) : RecyclerView.ViewHolder(itemView) {

        lateinit var ivLogoFC: ImageView
        lateinit var tvNameFC: TextView

        init {
            ivLogoFC = itemView.findViewById(FootballClubItemUI.ivLogoFC)
            tvNameFC = itemView.findViewById(FootballClubItemUI.tvNameFC)
        }

    }
}