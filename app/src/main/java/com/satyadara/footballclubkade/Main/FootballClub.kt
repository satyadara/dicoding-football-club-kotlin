package com.satyadara.fcapp

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FootballClub(val name: String?, val logo: Int ) : Parcelable {
}