package com.satyadara.footballclubkade.Main

import android.content.Context
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import com.satyadara.footballclubkade.R
import org.jetbrains.anko.*

class FootballClubItemUI : AnkoComponent<Context> {
    companion object {
        const val ivLogoFC = 1
        const val tvNameFC = 2
        const val logoDipSize = 60
    }
    override fun createView(ui: AnkoContext<Context>): View = with(ui){
        linearLayout {
            lparams(width = matchParent, height = wrapContent)
            padding = dip(16)
            orientation = LinearLayout.HORIZONTAL

            imageView {
                id = ivLogoFC
                setImageResource(R.drawable.img_barca)
            }.lparams(dip(logoDipSize), dip(logoDipSize))

            textView {
                id = tvNameFC
                text = "Barca"
                textSize = sp(16).toFloat()
                padding = dip(5)
                gravity = Gravity.CENTER_VERTICAL
            }.lparams(width = wrapContent, height = wrapContent)
        }
    }
}